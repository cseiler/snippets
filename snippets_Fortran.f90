!>\file
!!
!> \section Constants Constants
!! Constants cannot be altered during the execution of a program.
!! There are two types of constants:
!! - Literal constants (value but no name)
!! - Named constants (value and name)
!!
!! Named constants must be declared just like a variable
!!
!> \include snippets/constants.f90
!!
!> \section types Data types
!! Theere are five different data types:
  !! - Integer type (e.g. 1,2,3)
  !! - Real type (e.g. 1.1,1.2,1.3)
  !! - Complex type (e.g. (3.1, -5.2))
  !! - Logical type (.true, .false)
  !! - Character type (e.g. xyz)
!!
!> \include snippets/data_types.f90
!!
!> \section Decision Decision making constructs
!! Decision making constructs include:
!! - if... then ... end if
!! - if... then... else
!! - nested if construct
!! - select case construct
!! - nested select case construct
!!
!> \subsection Operators Operators
!! Decision making constructs often use operators.
!! There are three types of operators:
!! -# Arithmetic Operators
!!    - +,-,*,/,**
!! -# Relational Operators
!!    - ==, /=, >, <, >=, <=
!! -# Logical Operators
!!    - .and., .or., .not., .eqv., neqv.
!!
!> \include snippets/decisions.f90
!!
!> \section Loops Loop types
!! - do loop
!! - do while loop
!! - nested loops
!> \subsection Control Control statements
!! - exit
!! - cycle
!!
!> \include snippets/loops.f90
!!
!> \section Procedures Procedures
!! - procedures are functions or subroutines
!! - functions take one or many parameters as inputs and return a single output value
!! - subroutines can modify the input variables and return several output values
!! - arguments inside functions and subroutines must be declared with:
!!  - intent(in): input
!!  - intent(out): output
!!  - intent(inout): variable comes in with a value and leaves with a value
!!
!> \subsection Function Function
!! - intrinsic functions: e.g. sin(x)
!! - external functions: defined by user
!> \include function.f90
!!
!> \subsection Subroutine Subroutine
!! - subroutines are invoked with a call statement
!> \include snippets/subroutine.f90
!!
!> \section Arrays Arrays
!! - declare dimensions (e.g. 3 x 4 2D-matrix)
!! - Assign values
!!
!> \include snippets/array.f90
!! \subsection terms Some array related terms
!! - rank: number of dimensions an array has (example: the rank of array2D above is 2)
!! - extent: number of elements along a dimension (example: the extent of array2D above is 3 in the i dimension and 4 in the j dimension)
!! - shape: vector of the extent of each dimension (example: the shape of array2D is (3,4))
!! - size: number of elements, i.e. the product of the extents of all dimensions (example: the size of array2D above is 12)
!!
!> \section procedure Passing arrays to procedures
!! - an array can be passed to a procedure as an argument
!!
!> \include snippets/array_procedure.f90
!!
