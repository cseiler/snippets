program array_procedure
  implicit none
  integer,dimension (10) :: myarray
  integer::i

  interface
  subroutine fillArray(a)
  integer, dimension(:), intent(out)::a
  integer::i
  end subroutine fillArray

  subroutine printArray(a)
  integer,dimension(:)::a
  integer::i
  end subroutine printArray
  end interface

  call fillArray(myarray)
  call printArray(myarray)

end program array_procedure

subroutine fillArray(a)
implicit none
  integer,dimension(:), intent(out)::a
  integer::i,arraySize
  arraySize=size(a)
  do i=1,arraySize
    a(i)=i
  end do
end subroutine fillArray

subroutine printArray(a)
implicit none
  integer,dimension(:)::a
  integer::i,arraySize
  arraySize=size(a)
  do i=1,arraySize
    print*,a(i)
  end do
end subroutine printArray
