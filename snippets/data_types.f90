program datatypes
  implicit none
  integer :: int
  real :: rea
  complex :: com
  logical :: log
  character(len=40) :: name

  int = 5
  rea = 5.3
  com = (2.2,5.3)
  log = .true.
  name = "seiler"

  print *, int
  print *, rea
  print *, com
  print *, log
  print *, name

end program datatypes
