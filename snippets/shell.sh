# hide directory
export PS1='\u@\h: '

# list first four files
ls | head -4

# find a file that contains a given string
grep -r "mystring" ./

# find a file with the name myfilename
find . -name "myfilename"

# install locate
sudo apt-get install mlocate

# update data base used by locate
sudo updatedb

# show CPU
cat /proc/cpuinfo

# replace text
for i in aaa bbb 
do  
  cd $i  
  sed -i "s|xxx|${i}|g" text.sh  
  cd ../ 
done

