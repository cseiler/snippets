program loops

  implicit none
  integer :: i ! variable i is the loop counter (must be integer)
  integer :: n = 1
  integer :: m = 1

  ! Loop (1): do loop with an interval of 1
  do i=-15, 10 ! lower and upper bound
    print *,i
  end do
  !
  ! Loop (2): do loop with an interval of 10
  do i=-100, 100, 10 ! lower, upper bound, interval
    print *,i
  end do
  !
  ! Loop (3): do while loop
   do while (n <= 10)
      n = n + 1
      print*,  n
   end do
   !
   ! Loop (4): exit statement
   do
      m = m + 1
      print*,  m
      if (m > 100) then
        exit
      end if
   end do
   !
   ! Loop (5): cycle statement
   do  i = 1, 10
      if ( i == 5 ) then
         cycle ! Skip when i == 5
      endif
      print*,  i
   end do

end program loops
