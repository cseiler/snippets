!>\file
!!
!> \section data vectors, matrices, arrays, lists, data frames
!> \subsection subset
!> \include snippets/subset.R
!> \subsection sort
!> \include snippets/sort.R
!> \subsection array
!> \include snippets/array.R
!!
!> \section statistics statistics
!> \subsection regression
!> \include snippets/regression.R
!> \subsection Wilcox
!> \include snippets/wilcox.R
!> \subsection modal modal value
!> \include snippets/modal_value.R
!!
!> \section processing data processing
!> \subsection loopstring loop with strings
!> \include snippets/loop_strings.R
!> \subsection parallel run code in parallel
!> \include snippets/parallel.R
!> \subsection rbind rbind using list pattern
!> \include snippets/rbind_list_pattern.R
!> \subsection tapply
!> \include snippets/tapply.R
!!
!> \section netcdf netcdf
!> \subsection ncopen
!> \include snippets/import_netcdf.R
!> \subsection netcdf create read plot netcdf
!> \include snippets/create_read_plot_netcdf.R
!> \subsection netcdf_array convert netcdf to array
!> \include snippets/netcdf_array.R
!> \subsection projection get projection
!> \include snippets/get_projection.R
!!
!> \section raster raster
!> \subsection NA
!> \include snippets/NA.R
!> \subsection longitude
!> \include snippets/longitude_360_vs_180.R
!> \subsection count count points in grid cell
!> \include snippets/count.R
!> \subsection seasonal seasonal mean
!> \include snippets/seasonal_mean.R
!> \subsection zonal zonal mean
!> \include snippets/zonal_mean.R
!> \subsection percentiles
!> \include snippets/percentiles.R
!!
!> \section polygon polygon
!> \subsection polar polar stereographic projection
!> \include snippets/polar_stereographic.R
!> \subsection reproject reproject polygon
!> \include snippets/reproject_polygon.R
!> \subsection rotate rotate polygon
!> \include snippets/rotate_polygon.R
!!
!> \section points points
!> \subsection points
!> \include snippets/spatial_points.R
!!
!> \section plots plots
!> \subsection fonts
!> \include snippets/fonts.R
!> \subsection subplots
!> \include snippets/subplots.R
!> \subsection hatching
!> \include snippets/hatching.R
!> \subsection subscripts superscripts and subscripts
!> \include snippets/superscript_subscript.R
!> \subsection colors transparent colors
!> \include snippets/transparent_colors.R
