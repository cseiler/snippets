program array
  implicit none
  ! Declare dimensions:
  real :: array1D(5) ! 1D aray with 5 elements
  integer :: array2D(3,4) ! 2D array with 12 elements
  integer :: i,j
  ! (1) array1D
  ! Assign values for array1D, option (1):
  array1D= (/1,2,3,4,5/)
  print*,array1D
  ! Assign values for array1D, option (2):
  do i=1,5
    array1D(i) = i
  end do
  !
  print*,array1D
  ! (2) array2D
  ! Assign values for array2D:
  do i=1,3
    do j = 1,4
      array2D(i,j)=i+j
    end do
  end do
  print*,array2D
end program
