!>\file
!!
!> \section my-section-name Sections
!> \subsection my-subsection-name This is a subsection
!> \subsubsection my-subsubsection-name This is a subsubsection
!! The first string after the section command gives the section namme.
!! All strings after that belong to the section title.
!> \section LaTex Equations
!! LaTex equations may be included in the running text using "\f$": \f$y=\sqrt{x}\f$.
!! To center an equation in a separate line one must use "\f[", instead:
!! \f[ y=\sqrt{x}. \f]
!> \section Citations Citations
!! Citations can be included like this: \cite Arora2011-79f.
!! The corresponding .bib file is specified in the Doxyfile (CITE_BIB_FILES).
!> \section Links Links
!! This is how to add a [link](https://cseiler.bitbucket.io/).
!> \section Code Code
!! This is how to include a chunk of code:
!> \code{.f}
!! program addnumbers
!!  implicit none
!!  real :: a,b,result
!!  a = 2.0
!!  b = 3.0
!!  result = a+b
!!  print *, 'Two plus three gives ', result
!! end program addnumbers
!> \endcode

program addnumbers
  implicit none

  ! Type declarations
  real :: a,b,result

  a = 2.0
  b = 3.0
  result = a+b

  print *, 'Two plus three gives ', result
end program addnumbers
