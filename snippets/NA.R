# Set values below 0 to NA:
fun <- function(x) { x[x<0] <- NA; return(x) }

# Set NA to 0:
fun <- function(x) { x[is.na(x)] <- 0; return(x)}
