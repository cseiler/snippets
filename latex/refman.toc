\contentsline {chapter}{\numberline {1}Content}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}atom}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}doxygen}{1}{section.1.2}
\contentsline {section}{\numberline {1.3}gitlab}{1}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}Create a new website}{1}{subsection.1.3.1}
\contentsline {chapter}{\numberline {2}snippets}{3}{chapter.2}
\contentsline {chapter}{\numberline {3}File Index}{5}{chapter.3}
\contentsline {section}{\numberline {3.1}File List}{5}{section.3.1}
\contentsline {chapter}{\numberline {4}File Documentation}{7}{chapter.4}
\contentsline {section}{\numberline {4.1}doxygen\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}basics.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}F90 File Reference}{7}{section.4.1}
\contentsline {subsection}{\numberline {4.1.1}Detailed Description}{7}{subsection.4.1.1}
\contentsline {subsection}{\numberline {4.1.2}Sections}{7}{subsection.4.1.2}
\contentsline {subsubsection}{\numberline {4.1.2.1}This is a subsection}{7}{subsubsection.4.1.2.1}
\contentsline {paragraph}{\numberline {4.1.2.1.1}This is a subsubsection}{7}{paragraph.4.1.2.1.1}
\contentsline {subsection}{\numberline {4.1.3}Equations}{7}{subsection.4.1.3}
\contentsline {subsection}{\numberline {4.1.4}Citations}{7}{subsection.4.1.4}
\contentsline {subsection}{\numberline {4.1.5}Links}{7}{subsection.4.1.5}
\contentsline {subsection}{\numberline {4.1.6}Code}{8}{subsection.4.1.6}
\contentsline {section}{\numberline {4.2}snippets\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Fortran.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}F90 File Reference}{8}{section.4.2}
\contentsline {subsection}{\numberline {4.2.1}Detailed Description}{8}{subsection.4.2.1}
\contentsline {subsection}{\numberline {4.2.2}Constants}{8}{subsection.4.2.2}
\contentsline {subsection}{\numberline {4.2.3}Data types}{8}{subsection.4.2.3}
\contentsline {subsection}{\numberline {4.2.4}Decision making constructs}{9}{subsection.4.2.4}
\contentsline {subsubsection}{\numberline {4.2.4.1}Operators}{9}{subsubsection.4.2.4.1}
\contentsline {subsection}{\numberline {4.2.5}Loop types}{9}{subsection.4.2.5}
\contentsline {subsubsection}{\numberline {4.2.5.1}Control statements}{10}{subsubsection.4.2.5.1}
\contentsline {section}{\numberline {4.3}snippets\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\_\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}R.\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}F90 File Reference}{10}{section.4.3}
\contentsline {subsection}{\numberline {4.3.1}Detailed Description}{10}{subsection.4.3.1}
\contentsline {subsection}{\numberline {4.3.2}vectors, matrices, arrays, lists, data frames}{10}{subsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.2.1}subset}{10}{subsubsection.4.3.2.1}
\contentsline {subsubsection}{\numberline {4.3.2.2}sort}{11}{subsubsection.4.3.2.2}
\contentsline {subsubsection}{\numberline {4.3.2.3}array}{11}{subsubsection.4.3.2.3}
\contentsline {subsection}{\numberline {4.3.3}statistics}{11}{subsection.4.3.3}
\contentsline {subsubsection}{\numberline {4.3.3.1}regression}{11}{subsubsection.4.3.3.1}
\contentsline {subsubsection}{\numberline {4.3.3.2}Wilcox}{11}{subsubsection.4.3.3.2}
\contentsline {subsubsection}{\numberline {4.3.3.3}modal value}{11}{subsubsection.4.3.3.3}
\contentsline {subsection}{\numberline {4.3.4}data processing}{11}{subsection.4.3.4}
\contentsline {subsubsection}{\numberline {4.3.4.1}loop with strings}{11}{subsubsection.4.3.4.1}
\contentsline {subsubsection}{\numberline {4.3.4.2}run code in parallel}{12}{subsubsection.4.3.4.2}
\contentsline {subsubsection}{\numberline {4.3.4.3}rbind using list pattern}{12}{subsubsection.4.3.4.3}
\contentsline {subsubsection}{\numberline {4.3.4.4}tapply}{12}{subsubsection.4.3.4.4}
\contentsline {subsection}{\numberline {4.3.5}netcdf}{12}{subsection.4.3.5}
\contentsline {subsubsection}{\numberline {4.3.5.1}ncopen}{12}{subsubsection.4.3.5.1}
\contentsline {subsubsection}{\numberline {4.3.5.2}netcdf}{13}{subsubsection.4.3.5.2}
\contentsline {subsubsection}{\numberline {4.3.5.3}convert netcdf to array}{13}{subsubsection.4.3.5.3}
\contentsline {subsubsection}{\numberline {4.3.5.4}get projection}{13}{subsubsection.4.3.5.4}
\contentsline {subsection}{\numberline {4.3.6}raster}{14}{subsection.4.3.6}
\contentsline {subsubsection}{\numberline {4.3.6.1}NA}{14}{subsubsection.4.3.6.1}
\contentsline {subsubsection}{\numberline {4.3.6.2}longitude}{14}{subsubsection.4.3.6.2}
\contentsline {subsubsection}{\numberline {4.3.6.3}count points in grid cell}{14}{subsubsection.4.3.6.3}
\contentsline {subsubsection}{\numberline {4.3.6.4}seasonal mean}{14}{subsubsection.4.3.6.4}
\contentsline {subsubsection}{\numberline {4.3.6.5}zonal mean}{14}{subsubsection.4.3.6.5}
\contentsline {subsubsection}{\numberline {4.3.6.6}percentiles}{14}{subsubsection.4.3.6.6}
\contentsline {subsection}{\numberline {4.3.7}polygon}{15}{subsection.4.3.7}
\contentsline {subsubsection}{\numberline {4.3.7.1}polar stereographic projection}{15}{subsubsection.4.3.7.1}
\contentsline {subsubsection}{\numberline {4.3.7.2}reproject polygon}{16}{subsubsection.4.3.7.2}
\contentsline {subsubsection}{\numberline {4.3.7.3}rotate polygon}{16}{subsubsection.4.3.7.3}
\contentsline {subsection}{\numberline {4.3.8}points}{16}{subsection.4.3.8}
\contentsline {subsubsection}{\numberline {4.3.8.1}points}{16}{subsubsection.4.3.8.1}
\contentsline {subsection}{\numberline {4.3.9}plots}{17}{subsection.4.3.9}
\contentsline {subsubsection}{\numberline {4.3.9.1}fonts}{17}{subsubsection.4.3.9.1}
\contentsline {subsubsection}{\numberline {4.3.9.2}subplots}{17}{subsubsection.4.3.9.2}
\contentsline {subsubsection}{\numberline {4.3.9.3}hatching}{17}{subsubsection.4.3.9.3}
\contentsline {subsubsection}{\numberline {4.3.9.4}superscripts and subscripts}{18}{subsubsection.4.3.9.4}
\contentsline {subsubsection}{\numberline {4.3.9.5}transparent colors}{18}{subsubsection.4.3.9.5}
